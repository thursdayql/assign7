
public class Utility
{

  // Scales the histogram so the largest * line is 60
  final double SCALE = 6.75;
  
  public String asteriskPrinter(int ArrayNum)
  {
    String str = "";

    for (int i = 0; i < ArrayNum/SCALE - 1; i++)
      str += "*";

    int asteriskCount = (int) (ArrayNum/SCALE);
    return str + " " + "(" + asteriskCount + ")";
  }
}

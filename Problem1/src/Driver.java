
//********************************************************************
//File:         Driver.java       
//Author:       Liam Quinn
//Date:         November 7th 2017
//Course:       CPS100
//
//Problem Statement:
// Write a program that creates a histogram that allows you to visually
// inspect the frequency distribution of a set of values. The program
// should read in an arbitrary number of integers from a text input file
// that are in the range 1 to 100 inclusive; then produce a chart similar to
// the one below that indicates how many input values fell in the range 1 
// to 10, 11, to 20, and so on. Print on asterisk for each value entered.
// 
//Inputs: Text File
//Outputs:  Histogram of the count of integers between 1-100
//  
//********************************************************************

import java.io.*;
import java.util.*;


public class Driver
{

  public static void main(String[] args) throws IOException
  {
    Scanner scan = new Scanner(new File("realdata.txt"));
    final int DIVIDER = 10;

    int[] numbers = new int[10];
    int retriever;

    Utility util = new Utility();

    while (scan.hasNextLine())
    {
      retriever = scan.nextInt();
      retriever = retriever / DIVIDER;
      // System.out.println(retriever);
      numbers[retriever]++;
    }

    // System.out.println(Arrays.toString(numbers));

    System.out.println("Histogram of Integers in Text File: ");
    System.out.println("  1-10: " + (util.asteriskPrinter(numbers[0])));
    System.out.println(" 11-20: " + (util.asteriskPrinter(numbers[1])));
    System.out.println(" 21-30: " + (util.asteriskPrinter(numbers[2])));
    System.out.println(" 31-40: " + (util.asteriskPrinter(numbers[3])));
    System.out.println(" 41-50: " + (util.asteriskPrinter(numbers[4])));
    System.out.println(" 51-60: " + (util.asteriskPrinter(numbers[5])));
    System.out.println(" 61-70: " + (util.asteriskPrinter(numbers[6])));
    System.out.println(" 71-80: " + (util.asteriskPrinter(numbers[7])));
    System.out.println(" 81-90: " + (util.asteriskPrinter(numbers[8])));
    System.out.println("91-100: " + (util.asteriskPrinter(numbers[9])));

    scan.close();
  }

}
